package com.dota.menu;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dota.menu.controllers.contrib.ScreensController;

public class MainApp extends Application {

	public static void main(String[] args) throws Exception {
		launch(args);
	}

	public void start(Stage stage) throws Exception {
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"beans.xml");
		BeanFactory factory = context;

		ScreensController screensController = (ScreensController) factory
				.getBean("screensController");
		
		Group root = new Group();
		root.getChildren().addAll(screensController);
		stage.setScene(new Scene(root, 900, 750));
		
		stage.show();
	}
}
