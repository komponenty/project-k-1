package com.dota.menu.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.dota.menu.controllers.contrib.ControlledScreen;
import com.dota.menu.controllers.contrib.ScreensController;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class MenuMainController implements Initializable, ControlledScreen {

	private ScreensController myController;

	@FXML
	public void startGame() {
		this.myController.setScreen("GameController");
	}
	
	@FXML
	public void gameEditor() {
		this.myController.setScreen("MenuEditorController");
	}
	
	@FXML
	public void gameSettings() {
		this.myController.setScreen("MenuSettingsController");
	}
	
	@FXML
	public void gameQuit() {
		Platform.exit();
	}
	
	@Override
	public void setScreenParent(ScreensController screenPage) {
		this.myController = screenPage;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}
}
