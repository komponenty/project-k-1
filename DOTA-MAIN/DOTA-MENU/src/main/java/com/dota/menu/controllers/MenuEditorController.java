package com.dota.menu.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.dota.menu.controllers.contrib.ControlledScreen;
import com.dota.menu.controllers.contrib.ScreensController;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;


public class MenuEditorController implements Initializable, ControlledScreen {

	private ScreensController myController;
	
	@FXML
	protected void backButton() {
		this.myController.setScreen("MenuMainController");
	}
	
	@FXML
	protected void listViewClicked() {
		
	}
	
	@FXML
	protected void saveButton() {
		
	}
	
	@FXML
	protected void addTextButton() {
		
	}
	
	@FXML
	protected void addPhotoButton() {
		
	}
	
	@FXML
	protected void addColorEffectButton() {
		
	}
	
	@Override
	public void setScreenParent(ScreensController screenPage) {
		this.myController = screenPage;
		
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
	

}
