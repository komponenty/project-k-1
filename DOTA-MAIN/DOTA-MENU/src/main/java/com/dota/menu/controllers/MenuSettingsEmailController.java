package com.dota.menu.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import com.dota.menu.controllers.contrib.ControlledScreen;
import com.dota.menu.controllers.contrib.ScreensController;
import com.mailsender.contract.SendEmail;

public class MenuSettingsEmailController implements Initializable, ControlledScreen {

	private ScreensController myController;
	@FXML 
	private TextField textField;
	@FXML 
	private TextField textField2;
	@FXML
	public void backButton() {
		this.myController.setScreen("MenuSettingsController");
	}
	
	@FXML
	public void sendButton() {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"beans.xml");
		BeanFactory factory = context;

		SendEmail mcomponent = (SendEmail)factory.getBean("mailMail");
		try {
		mcomponent.sendMail(this.textField2.getText(), this.textField.getText(), "Zaproszenie", 
				"Zaproszenie do gry w DOTA-KOMPONENTOWE - https://bitbucket.org/komponenty/project-k-1");
		}
		
		catch (Exception e) {
			
		}
		this.myController.setScreen("MenuMainController");
	}
	
	@Override
	public void setScreenParent(ScreensController screenPage) {
		this.myController = screenPage;
		
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

}
