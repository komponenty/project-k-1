package com.dota.menu.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.dota.board.GameBoard;
import com.dota.menu.controllers.contrib.ControlledScreen;
import com.dota.menu.controllers.contrib.ScreensController;
import com.dota.units.Unit1;
import com.dota.units.Unit2;
import com.dota.units.Unit3;
import com.dota.units.Unit4;
import com.game.main.GameMain;






import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class GameControlPanelController implements Initializable, ControlledScreen {

	private ScreensController myController;

	@FXML 
	protected void backButton() {
		this.myController.setScreen("MenuMainController");
	}
	
	@SuppressWarnings("rawtypes")
	@FXML private ListView listBox;
	
	@FXML private Button start;
	@FXML private Label status;
	@FXML private Button buttonBuy1; // 1
    @FXML private Button buttonBuy2; // 2
    @FXML private Button buttonBuy3; // 3
    @FXML private Button buttonBuy4; // 4
    
    @FXML private Button buttonRange1; // 5
    @FXML private Button buttonRange2; // 6
    @FXML private Button buttonRange3; // 7
    @FXML private Button buttonRange4; // 8
    
    @FXML private Button buttonAttack1; // 9
    @FXML private Button buttonAttack2; // 10
    @FXML private Button buttonAttack3; // 11
    @FXML private Button buttonAttack4; // 12
            
    @FXML private Button buttonDefense1; // 13
    @FXML private Button buttonDefense2; // 14
    @FXML private Button buttonDefense3; // 15
    @FXML private Button buttonDefense4; // 16
    
    @FXML private ProgressBar healthBarBase;
    
    @FXML private Label labelAttack1;
    @FXML private Label labelAttack2;
    @FXML private Label labelAttack3;
    @FXML private Label labelAttack4;
    
    @FXML private Label labelDefense1;
    @FXML private Label labelDefense2;
    @FXML private Label labelDefense3;
    @FXML private Label labelDefense4;
    
    @FXML private Label labelRange1;
    @FXML private Label labelRange2;
    @FXML private Label labelRange3;
    @FXML private Label labelRange4;
    
    @FXML private Label labelAttackBase;
    @FXML private Label labelRangeBase;
    @FXML private Label labelDefenseBase;
    @FXML private ProgressIndicator progressBar1;
    @FXML private Label lab1;
    @FXML private Label lab2;
    @FXML private Label lab3;
    @FXML private Label lab4;
    @FXML private Pane pane;
    @FXML private TextField textBoxGold;
    ObservableList<String> items = FXCollections.observableArrayList ();
    
    @FXML
    protected void UpdateButtons() {
        // Kuku
    }
    
    @FXML
    protected void sendMessage() {
    	
    }
    
    protected void UpdateLabels() {
        this.textBoxGold.setText(Integer.toString(GameBoard.getInstance().getGold()));
        this.lab1.setText(com.dota.units.Unit1.getGoldCost() + " G");
        this.lab2.setText(com.dota.units.Unit2.getGoldCost() + " G");
        this.lab3.setText(com.dota.units.Unit3.getGoldCost() + " G");
        this.lab4.setText(com.dota.units.Unit4.getGoldCost() + " G");
    }
    
    public Boolean canBuy(double gold) {
    	if (GameBoard.getInstance().getGold() < gold && GameBoard.getInstance().getGold() - gold < 0) {
    		return false;
    	}
    	else {
    		return true;
    	}
    }
    
    @FXML
    protected void Unit1() {
    	if (this.canBuy(Unit1.getGoldCost())) {
    		GameBoard.getInstance().addUnit1();
    		this.items.add("Agent");
    		UpdateLabels();
    	}
    }
    
    @FXML
    protected void UnitAttack1() {
        com.dota.units.Unit1.setUnitAttack(com.dota.units.Unit1.getUnitAttack() + 25);
        this.labelAttack1.setText("Atak: " + com.dota.units.Unit1.getUnitAttack());
        Unit1.setGoldCost(Unit1.getGoldCost()+com.dota.units.Unit1.getUnitAttack()/5+com.dota.units.Unit1.getUnitAttackRange()*5+com.dota.units.Unit1.getUnitHealth()/5);
        UpdateLabels();
    }
    
    @FXML
    protected void UnitAttackRange1() {
    	 com.dota.units.Unit1.setUnitAttackRange(com.dota.units.Unit1.getUnitAttackRange() + 1);
    	 this.labelRange1.setText("Zasięg: " + com.dota.units.Unit1.getUnitAttackRange());
    	 Unit1.setGoldCost(Unit1.getGoldCost()+com.dota.units.Unit1.getUnitAttack()/5+com.dota.units.Unit1.getUnitAttackRange()*5+com.dota.units.Unit1.getUnitHealth()/5);
    	 UpdateLabels();
    }
    
    @FXML
    protected void UnitHealth1() {
    	com.dota.units.Unit1.setUnitHealth(com.dota.units.Unit1.getUnitHealth() + 25);
    	this.labelDefense1.setText("Obrona: " + com.dota.units.Unit1.getUnitHealth());
    	Unit1.setGoldCost(Unit1.getGoldCost()+com.dota.units.Unit1.getUnitAttack()/5+com.dota.units.Unit1.getUnitAttackRange()*5+com.dota.units.Unit1.getUnitHealth()/5);
    	
    	UpdateLabels();
    }
    
    @FXML
    protected void Unit2() {
    	if (this.canBuy(Unit1.getGoldCost())) {
    		GameBoard.getInstance().addUnit2();
    		this.items.add("Kosmita 1");
    		UpdateLabels();
    	}
    }
    
    @FXML
    protected void UnitAttack2() {
    	 com.dota.units.Unit2.setUnitAttack(com.dota.units.Unit2.getUnitAttack() + 25);
         this.labelAttack2.setText("Atak: " + com.dota.units.Unit2.getUnitAttack());
         Unit2.setGoldCost(Unit2.getGoldCost()+com.dota.units.Unit2.getUnitAttack()/5+com.dota.units.Unit2.getUnitAttackRange()*5+com.dota.units.Unit2.getUnitHealth()/5);
         UpdateLabels();
    }
    
    @FXML
    protected void UnitAttackRange2() {
    	com.dota.units.Unit2.setUnitAttackRange(com.dota.units.Unit2.getUnitAttackRange() + 1);
    	this.labelRange2.setText("Zasięg: " + com.dota.units.Unit2.getUnitAttackRange());
    	Unit2.setGoldCost(Unit2.getGoldCost()+com.dota.units.Unit2.getUnitAttack()/5+com.dota.units.Unit2.getUnitAttackRange()*5+com.dota.units.Unit2.getUnitHealth()/5);
    	UpdateLabels();
    }
    
    @FXML
    protected void UnitHealth2() {
    	com.dota.units.Unit2.setUnitHealth(com.dota.units.Unit2.getUnitHealth() + 25);
    	this.labelDefense2.setText("Obrona: " + com.dota.units.Unit2.getUnitHealth());
    	Unit2.setGoldCost(Unit2.getGoldCost()+com.dota.units.Unit2.getUnitAttack()/5+com.dota.units.Unit2.getUnitAttackRange()*5+com.dota.units.Unit2.getUnitHealth()/5);
    	UpdateLabels();
    }
    
    @FXML
    protected void Unit3() {
    	if (this.canBuy(Unit1.getGoldCost())) {
    		GameBoard.getInstance().addUnit3();
    		this.items.add("Vader");
    		UpdateLabels();
    	}
    }
    
    @FXML
    protected void UnitAttack3() {
    	 com.dota.units.Unit3.setUnitAttack(com.dota.units.Unit3.getUnitAttack() + 25);
         this.labelAttack3.setText("Atak: " + com.dota.units.Unit3.getUnitAttack());
         Unit3.setGoldCost(Unit3.getGoldCost()+com.dota.units.Unit3.getUnitAttack()/5+com.dota.units.Unit3.getUnitAttackRange()*5+com.dota.units.Unit3.getUnitHealth()/5);
         UpdateLabels();
    }
    
    @FXML
    protected void UnitAttackRange3() {
    	com.dota.units.Unit3.setUnitAttackRange(com.dota.units.Unit3.getUnitAttackRange() + 1);
    	this.labelRange3.setText("Zasięg: " + com.dota.units.Unit3.getUnitAttackRange());
    	Unit3.setGoldCost(Unit3.getGoldCost()+com.dota.units.Unit3.getUnitAttack()/5+com.dota.units.Unit3.getUnitAttackRange()*5+com.dota.units.Unit3.getUnitHealth()/5);
    	UpdateLabels();
    }
    
    @FXML
    protected void UnitHealth3() {
    	com.dota.units.Unit3.setUnitHealth(com.dota.units.Unit3.getUnitHealth() + 25);
    	this.labelDefense3.setText("Obrona: " + com.dota.units.Unit2.getUnitHealth());
    	Unit3.setGoldCost(Unit3.getGoldCost()+com.dota.units.Unit3.getUnitAttack()/5+com.dota.units.Unit3.getUnitAttackRange()*5+com.dota.units.Unit3.getUnitHealth()/5);
    	UpdateLabels();
    }
    
    @FXML
    protected void Unit4() {
    	if (this.canBuy(Unit1.getGoldCost())) {
    		GameBoard.getInstance().addUnit4();
    		this.items.add("Kommandos");
    		UpdateLabels();
    	}
    }
    
    @FXML
    protected void UnitAttack4() {
    	 com.dota.units.Unit4.setUnitAttack(com.dota.units.Unit4.getUnitAttack() + 25);
    	 this.labelAttack4.setText("Atak: " + com.dota.units.Unit4.getUnitAttack());
    	 Unit4.setGoldCost(Unit4.getGoldCost()+com.dota.units.Unit4.getUnitAttack()/5+com.dota.units.Unit4.getUnitAttackRange()*5+com.dota.units.Unit4.getUnitHealth()/5);
    	 UpdateLabels();
    }
    
    @FXML
    protected void UnitAttackRange4() {
    	com.dota.units.Unit4.setUnitAttackRange(com.dota.units.Unit4.getUnitAttackRange() + 1);
    	 this.labelRange4.setText("Zasięg: " + com.dota.units.Unit4.getUnitAttackRange());
    	 Unit4.setGoldCost(Unit4.getGoldCost()+com.dota.units.Unit4.getUnitAttack()/5+com.dota.units.Unit4.getUnitAttackRange()*5+com.dota.units.Unit4.getUnitHealth()/5);
    	 UpdateLabels();
    }
    
    @FXML
    protected void UnitHealth4() {
    	com.dota.units.Unit4.setUnitHealth(com.dota.units.Unit4.getUnitHealth() + 25);
    	this.labelDefense4.setText("Obrona: " + com.dota.units.Unit4.getUnitHealth());
    	Unit4.setGoldCost(Unit4.getGoldCost()+com.dota.units.Unit4.getUnitAttack()/5+com.dota.units.Unit4.getUnitAttackRange()*5+com.dota.units.Unit4.getUnitHealth()/5);
    	UpdateLabels();
    }

    @FXML
    protected void BaseAttack() {
    	GameBoard.getInstance().getCentrum().setUnitAttack(GameBoard.getInstance().getCentrum().getUnitAttack() + 25);
    	this.labelAttackBase.setText("Atak: " + GameBoard.getInstance().getCentrum().getUnitAttack());
    	Unit4.setGoldCost(Unit4.getGoldCost()+com.dota.units.Unit4.getUnitAttack()/5+com.dota.units.Unit4.getUnitAttackRange()*5+com.dota.units.Unit4.getUnitHealth()/5);
    	UpdateLabels();
    }
    
    @FXML
    protected void BaseAttackRange() {
    	GameBoard.getInstance().getCentrum().setUnitAttackRange(GameBoard.getInstance().getCentrum().getUnitAttackRange() + 1);
    	this.labelRangeBase.setText("Zasięg: " + GameBoard.getInstance().getCentrum().getUnitAttackRange());
    	UpdateLabels();
    }
    
    @FXML
    protected void BaseHealth() {
    	GameBoard.getInstance().getCentrum().setUnitCurrentHealth(GameBoard.getInstance().getCentrum().getUnitCurrentHealth() + 25);
    	GameBoard.getInstance().getCentrum().setUnitHealth(GameBoard.getInstance().getCentrum().getUnitHealth() + 25);
    	this.labelDefenseBase.setText("Obrona: " + GameBoard.getInstance().getCentrum().getUnitCurrentHealth());
    	UpdateLabels();
    }
    
    @FXML
    protected void StartGame() {
    	for(int i=0; i < GameBoard.getInstance().max; i++) {
    		GameBoard.getInstance().addUnit5();
    	}
    	GameBoard.getInstance().max+=10; // w następnej rundzie 10 jednostek przeciwników więcej
    	this.start.setDisable(true);
    	
    	new GameMain().Run(this.pane, this);
		//GameBoardXML.toXML(System.getProperty("user.home")+ "/gameboard.xml");
    	this.items.clear();
		
    }
    
    public void UpdateCenterHealth() {
    	if ((double)(GameBoard.getInstance().getCentrum().getUnitHealth() - GameBoard.getInstance().getCentrum().getUnitCurrentHealth())* 0.001 > 1) {
    		this.healthBarBase.setProgress(0);
    		this.status.setText("Przegrałeś");
    		this.start.setDisable(true);
    	}
    	
    	else {
    		this.healthBarBase.setProgress(1 - (double)(GameBoard.getInstance().getCentrum().getUnitHealth() - GameBoard.getInstance().getCentrum().getUnitCurrentHealth())* 0.001);
    		this.progressBar1.setProgress((double)(GameBoard.getInstance().getEnemyCentrum().getUnitHealth() - GameBoard.getInstance().getEnemyCentrum().getUnitCurrentHealth())* 0.001);
    		
    	}
    	
    	if ((double)(GameBoard.getInstance().getEnemyCentrum().getUnitHealth() - GameBoard.getInstance().getEnemyCentrum().getUnitCurrentHealth())* 0.001 > 1) {

    		this.status.setText("Wygrałeś");
    		this.start.setDisable(true);
    	}
    	
    	//System.out.print("ZYCIE" + (GameBoard.getInstance().getCentrum().getUnitCurrentHealth() * 0.001) + "\n");
    	//System.out.print("ZYCIE przeciwnika" + (GameBoard.getInstance().getEnemyCentrum().getUnitCurrentHealth() * 0.001) + "\n");
    	
    	//System.out.print((GameBoard.getInstance().getCentrum().getUnitCurrentHealth()));
    	//System.out.print((GameBoard.getInstance().getCentrum().getUnitHealth()));
    	//this.healthBarBase.setProgress(0.7);
    }
    
    @FXML
    protected void StartGame2() {
        System.out.print("START");
    }
    
	@SuppressWarnings("unchecked")
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.listBox.setItems(items);
	}

	@Override
	public void setScreenParent(ScreensController screenPage) {
		this.myController = screenPage;
	}

	public void endOne() {
		// TODO Auto-generated method stub
		this.start.setDisable(false);
		GameBoard.getInstance().getEnemyUnits().clear();
	}

	public void Refresh() {
		this.UpdateCenterHealth();
		Integer gold = GameBoard.getInstance().getGold();
		this.textBoxGold.setText(gold.toString());
		
		
	}
}