package com.dota.menu.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import com.dota.board.GameSettings;
import com.dota.menu.controllers.contrib.ControlledScreen;
import com.dota.menu.controllers.contrib.ScreensController;

public class MenuSettingsNetworkController implements Initializable, ControlledScreen{

	private ScreensController myController;
	@FXML 
	private TextField textField; 
	
	@FXML
	public void backButton() {
		this.myController.setScreen("MenuSettingsController");
	}
	
	@FXML
	public void saveButton() {
		GameSettings.getInstance().setIp(this.textField.getText());
	}
	
	@Override
	public void setScreenParent(ScreensController screenPage) {
		this.myController = screenPage;		
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}
}
