package com.dota.menu.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import com.dota.menu.controllers.contrib.ControlledScreen;
import com.dota.menu.controllers.contrib.ScreensController;

public class MenuSettingsController implements Initializable, ControlledScreen {

	private ScreensController myController;
	
	@FXML
	public void networkSettings() {
		this.myController.setScreen("MenuSettingsNetworkController");
	}
	
	@FXML
	public void sendEmail() {
		this.myController.setScreen("MenuSettingsEmailController");
	}
	
	@FXML
	public void backButton() {
		this.myController.setScreen("MenuMainController");
	}
	
	@Override
	public void setScreenParent(ScreensController screenPage) {
		this.myController = screenPage;
		
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

}
