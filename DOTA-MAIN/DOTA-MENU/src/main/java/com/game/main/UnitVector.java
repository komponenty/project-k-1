package com.game.main;

import org.newdawn.slick.Animation;

import com.dota.units.Unit;

public class UnitVector {
	public UnitVector(Unit unit2, Animation animation2) {
		this.unit = unit2;
		this.animation = animation2;
	}
	
	public Unit unit;
	public Animation animation;
}
