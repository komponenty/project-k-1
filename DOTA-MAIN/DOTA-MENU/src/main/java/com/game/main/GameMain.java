package com.game.main;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;





import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import javafx.scene.layout.Pane;

import com.dota.board.GameBoard;
import com.dota.board.GameBoardXML;
import com.dota.menu.controllers.GameControlPanelController;
import com.dota.units.Unit;
import com.dota.units.Unit1;
import com.dota.units.Unit2;
import com.dota.units.Unit3;
import com.dota.units.Unit4;
import com.dota.units.Unit5;
import com.game.contrib.UnitSpriteAnimation;

public class GameMain
{

	
	public GameMain()
	{
		
	}
	
	GameControlPanelController controller;
	AnimationTimer t;
	Pane panel;
	List<Integer> toDelete = new LinkedList<Integer>();
	List<Integer> toDelete2 = new LinkedList<Integer>();
	double x = 0;
	private List<ImageView> animations = new LinkedList<ImageView>();
	private List<ImageView> enemys = new LinkedList<ImageView>();
	public void Run(Pane pane, GameControlPanelController gameControlPanelController) {
		this.controller = gameControlPanelController;
		this.controller.UpdateCenterHealth();
		this.panel = pane;
		double x=0;
		for(int i=0; i < GameBoard.getInstance().getUnits().size(); i++) {
			//ImageView image = new ImageView(new Image("images/animations/u1s.png"));
			ImageView image = new ImageView(new Image(GameBoard.getInstance().getUnits().get(i).getSCHOOT()));
			UnitSpriteAnimation anim = new UnitSpriteAnimation(image, Duration.millis(400), 5, 8, 10, 10, 64, 64);
			anim.setCycleCount(Animation.INDEFINITE);
		    anim.play();
			animations.add(image);
			image.setX(x);
			x-=25;
			image.setY(500);
			pane.getChildren().add(image);
		}
		x = 650;
		for(int i=0; i < GameBoard.getInstance().getEnemyUnits().size(); i++) {
			//ImageView image = new ImageView(new Image("images/animations/u1s.png"));
			ImageView image = new ImageView(new Image(GameBoard.getInstance().getEnemyUnits().get(i).getSCHOOT()));
			UnitSpriteAnimation anim = new UnitSpriteAnimation(image, Duration.millis(400), 5, 8, 10, 10, 64, 64);
			anim.setCycleCount(Animation.INDEFINITE);
		    anim.play();
			enemys.add(image);
			image.setX(x);
			x+=25;
			image.setY(500);
			pane.getChildren().add(image);
		}
		
		t = new AnimationTimer() {
				@Override
				public void handle(long l) {
					//System.out.print(l + "\n");
					if ((a && d)) {
						this.stop();
					}
					Update(l);
					//this.stop();
				}
			};
		t.start();
	}
	
	Boolean a = false;
	Boolean d = false;
	public void Update(long l) {
		//a = true;
		//b = true;
		double b;
		for(int j=0;j < animations.size(); j++) {
			b = animations.get(j).getX();
			animations.get(j).setX(b + 0.5);
				for(int w=0;w < enemys.size(); w++) {
					double d = enemys.get(w).getX();
					if(Math.abs(b - d) < 40) {
						Random r = new Random();
						double r1 = r.nextInt(100);
						double r2 = r.nextInt(100);
						
						if(r1 > r2) {
							GameBoard.getInstance().getEnemyUnits().get(w).setUnitCurrentHealth(
									GameBoard.getInstance().getEnemyUnits().get(w).getUnitCurrentHealth()
										- GameBoard.getInstance().getUnits().get(j).getUnitAttack());
							
							if(GameBoard.getInstance().getEnemyUnits().get(w).getUnitCurrentHealth() <= 0) {
								this.toDelete.add(w);
							}
							
							
						}
						
						else {
							GameBoard.getInstance().getUnits().get(j).setUnitCurrentHealth(
									GameBoard.getInstance().getUnits().get(j).getUnitCurrentHealth()
										- GameBoard.getInstance().getEnemyUnits().get(w).getUnitAttack());
							
							if(GameBoard.getInstance().getUnits().get(j).getUnitCurrentHealth() <= 0) {
								this.toDelete2.add(j);
							}
						}
					}
				}
			
			if(b > 750) {
				GameBoard.getInstance().getEnemyCentrum().setUnitCurrentHealth(
						GameBoard.getInstance().getEnemyCentrum().getUnitCurrentHealth() - 
							GameBoard.getInstance().getUnits().get(j).getUnitAttack());
				
				if (GameBoard.getInstance().getEnemyCentrum().getUnitCurrentHealth() <= 0) {
					
				}
				
				if(animations.size() - 1 == j) {
					this.a = true;
				}
			}
		}
		double c;
		for(int j=0;j < enemys.size(); j++) {	
			c = enemys.get(j).getX();
			enemys.get(j).setX(c - 0.5);
			if(c < -50) {
				GameBoard.getInstance().getCentrum().setUnitCurrentHealth(
						GameBoard.getInstance().getCentrum().getUnitCurrentHealth() - 
							GameBoard.getInstance().getEnemyUnits().get(j).getUnitAttack());
				
				this.controller.UpdateCenterHealth();
				
				if (GameBoard.getInstance().getCentrum().getUnitCurrentHealth() <= 0) {
					
				}
				
				if(enemys.size() - 1 == j) {
					this.d = true;
				}

				
			}
		}
		
		if ((a && d) || this.enemys.isEmpty() || this.animations.isEmpty() ) {
			this.a=true;
			this.d=true;
			
			GameBoard.getInstance().getCentrum().setUnitCurrentHealth(GameBoard.getInstance().getCentrum().getUnitCurrentHealth() - this.enemys.size() * Unit5.getUnitAttack());
			GameBoard.getInstance().getEnemyCentrum().setUnitCurrentHealth(GameBoard.getInstance().getEnemyCentrum().getUnitCurrentHealth() - this.animations.size() * Unit1.getUnitAttack());
			//System.out.print("Moj atak" + Unit1.getUnitAttack());
			//System.out.print(GameBoard.getInstance().getCentrum().getUnitCurrentHealth() - this.enemys.size() * Unit5.getUnitAttack() + "\n");
			//System.out.print("Przeciwnik atak:" + Unit5.getUnitAttack());
			this.panel.getChildren().clear();
			this.animations.clear();
			this.enemys.clear();
			//System.out.print(this.panel.getChildren().size());
			this.controller.endOne();
		}
		
		for(int p=0; p < this.toDelete.size(); p++) {
			this.enemys.remove(p);
			this.panel.getChildren().remove(this.panel.getChildren().size() - this.enemys.size() - 1);
			GameBoard.getInstance().setGold(GameBoard.getInstance().getGold() + 40);
		}
		for(int p=0; p< this.toDelete2.size(); p++) {
			this.animations.remove(p);
			this.panel.getChildren().remove(p);
		}
		this.toDelete.clear();
		this.toDelete2.clear();
		this.controller.Refresh();
	}
}