package com.game.main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.dota.board.GameBoard;
import com.dota.board.GameBoardXML;
import com.dota.units.Unit;


public class GameProgress {
	private static volatile GameProgress instance = null;

	public static GameProgress getInstance() throws SlickException{
		if (instance == null) {
			synchronized (GameProgress.class) {
				if (instance == null) {
					instance = new GameProgress();
				}
			}
		}
		return instance;
	}
	
	private GameProgress() throws SlickException {
		this.inGame = true;
		this.startGame = true;
		this.animations = new LinkedList<UnitVector>();
		this.enemyAnimations = new LinkedList<UnitVector>();
		//System.out.print(this.animations);
		this.background = new Image("images/backgrounds/background_1.jpg"); //The background
		this.toDelete = new LinkedList<Unit>();
		
		try {
			GameBoardXML.fromXML(System.getProperty("user.home") + "/gameboard.xml");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.commandCenter_1 = new Image(GameBoard.getInstance().getImagesDirectory().concat("/icons/CommandCentrum_1.png"));
		this.commandCenter_2 = new Image(GameBoard.getInstance().getImagesDirectory().concat("/icons/CommandCentrum_2.png"));
		
		this.inGame = true;
		this.startGame = true;
	}
	
	Image background;
	Image commandCenter_1;
	Image commandCenter_2;
	
	public List<UnitVector> animations;
	public List<Unit> toDelete;
	public boolean inGame;
	public boolean startGame;
	public List<UnitVector> enemyAnimations;
}
