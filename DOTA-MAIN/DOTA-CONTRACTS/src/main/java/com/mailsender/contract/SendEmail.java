/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mailsender.contract;

/**
 *
 * @author Zofia
 */
public interface SendEmail {
    void sendMail(final String from, final String to, final String subject, final String text);
    void sendMailAttachment(final String from, final String to, final String subject, final String text, 
            final String attachmentPath, final String attachmentName);
}
