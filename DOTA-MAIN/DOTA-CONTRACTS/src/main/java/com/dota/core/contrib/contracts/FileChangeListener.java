package com.dota.core.contrib.contracts;

import java.io.File;

/**
 * Listener interested in {@link File} changes.
 * 
 * @author Pascal Essiembre
 */
public interface FileChangeListener {
	  public void fileChanged(File file);
}