package com.dota.board;

import java.util.LinkedList;
import java.util.List;

public class GameSettings {
	private static volatile GameSettings instance = null;

	private String ip;
	private List<String> messages;
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public static GameSettings getInstance(){
		if (instance == null) {
			synchronized (GameSettings.class) {
				if (instance == null) {
					instance = new GameSettings();
				}
			}
		}
		return instance;
	}
	
	private GameSettings() {
		this.messages = new LinkedList<String>();
	}

	public List<String> getMessages() {
		return messages;
	}
}
