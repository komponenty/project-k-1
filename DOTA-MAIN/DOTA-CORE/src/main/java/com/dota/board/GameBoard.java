package com.dota.board;

import java.io.Serializable;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dota.board.GameBoard;
import com.dota.units.CommandCentrum;
import com.dota.units.Unit;
import com.dota.units.Unit1;
import com.dota.units.Unit2;
import com.dota.units.Unit3;
import com.dota.units.Unit4;
import com.dota.units.Unit5;

@SuppressWarnings("serial")
@Component("GameBoard")
public class GameBoard implements Serializable {
	
	private static volatile GameBoard instance = null;

	@Autowired
	public static GameBoard getInstance(){
		if (instance == null) {
			synchronized (GameBoard.class) {
				if (instance == null) {
					instance = new GameBoard();
				}
			}
		}
		return instance;
	}
	private int count = 0;
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	private LinkedList<Unit> units;
	private LinkedList<Unit> enemyUnits;
	private CommandCentrum centrum;
	private CommandCentrum enemyCentrum;
	private int gold;
	public int max;
	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	private GameBoard() {
		this.units = new LinkedList<Unit>();
		this.enemyUnits = new LinkedList<Unit>();
		this.centrum = new CommandCentrum(getBackgroundsDirectory().concat("/background_1.jpg"));
		this.centrum.setUnitCurrentHealth(10000);
		this.centrum.setUnitHealth(10000);
		this.enemyCentrum = new CommandCentrum(getBackgroundsDirectory().concat("/background_2.jpg"));
		this.enemyCentrum.setUnitCurrentHealth(10000);
		this.enemyCentrum.setUnitHealth(10000);
		this.gold = 1000;
		this.max = 10;
	}

	public String getAnimationsDirectory() {
		return this.getImagesDirectory().concat("/animations");
	}

	public String getBackgroundsDirectory() {
		return this.getImagesDirectory().concat("/backgrounds");
	}

	public CommandCentrum getCentrum() {
		return centrum;
	}

	public CommandCentrum getEnemyCentrum() {
		return enemyCentrum;
	}

	public LinkedList<Unit> getEnemyUnits() {
		return this.enemyUnits;
	}

	public String getImagesDirectory() {
		return "images";
	}

	public LinkedList<Unit> getUnits() {
		return this.units;
	}

	public void setCentrum(CommandCentrum centrum) {
		this.centrum = centrum;
	}

	public void setEnemyCentrum(CommandCentrum enemyCentrum) {
		this.enemyCentrum = enemyCentrum;
	}

	public void setEnemyUnits(LinkedList<Unit> enemyUnits) {
		this.enemyUnits = enemyUnits;
	}

	public void setUnits(LinkedList<Unit> units) {
		this.units = units;
	}

	public void addUnit1() {
		this.count += 1;
		Unit unit = new Unit1();
		unit.setX(unit.getX() - count  * 35);
		this.units.add(unit);	
		this.gold = this.gold - Unit1.getGoldCost();
		
	}

	public void addUnit2() {
		this.count += 1;
		Unit unit = new Unit3();
		unit.setX(unit.getX() - count  * 35);
		this.units.add(unit);
		this.gold = this.gold - Unit2.getGoldCost();
	}
	
	public void addUnit3() {
		this.count += 1;
		Unit unit = new Unit2();
		unit.setX(unit.getX() - count  * 35);
		this.units.add(unit);
		this.gold = this.gold - Unit3.getGoldCost();

	}
	
	public void addUnit4() {
		this.count += 1;
		Unit unit = new Unit4();
		unit.setX(unit.getX() - count  * 35);
		this.units.add(unit);
		this.gold = this.gold - Unit4.getGoldCost();

	}
	
	public void addUnit5() {
		this.count += 1;
		Unit unit = new Unit5();
		unit.setX(unit.getX() + count  * 35);
		this.enemyUnits.add(unit);
		}
	
	public static void setInstance(Object readObject) {
		GameBoard.instance = (GameBoard)readObject;
	}
}
