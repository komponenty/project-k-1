package com.dota.units;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CommandCentrum implements Serializable {

	protected int unitAttack = 0;
	protected int unitAttackRange = 0;

	public int getUnitAttack() {
		return unitAttack;
	}

	public int getUnitAttackRange() {
		return unitAttackRange;
	}

	public void setUnitAttack(int unitAttack) {
		this.unitAttack = unitAttack;
	}

	public void setUnitAttackRange(int unitAttackRange) {
		this.unitAttackRange = unitAttackRange;
	}

	protected int unitHealth = 10000;
	protected int unitCurrentHealth = 10000;
	protected String centrumImage;

	public CommandCentrum(String centrumImage) {
		super();
		this.centrumImage = centrumImage;
	}

	public String getCentrumImage() {
		return centrumImage;
	}

	public int getUnitCurrentHealth() {
		return unitCurrentHealth;
	}

	public int getUnitHealth() {
		return unitHealth;
	}

	public void setCentrumImage(String centrumImage) {
		this.centrumImage = centrumImage;
	}

	public void setUnitCurrentHealth(int unitCurrentHealth) {
		this.unitCurrentHealth = unitCurrentHealth;
	}

	public void setUnitHealth(int unitHealth) {
		this.unitHealth = unitHealth;
	}
}
