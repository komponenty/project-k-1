package com.dota.units;

import com.dota.board.GameBoard;

@SuppressWarnings("serial")
public class Unit5 extends Unit  {
	
	protected static int goldCost = 0;
	protected static int unitAttack = 5;
	protected static int unitAttackRange = 1;
	protected static int unitHealth = 15;
	protected int unitCurrentHealth = 0;
	
	public Unit5() {
		super();
		this.unitAttack = 5;
		this.unitCurrentHealth = Unit5.unitHealth;
		this.number = 5;
		WALK = GameBoard.getInstance().getAnimationsDirectory() + "/u5w.png"; //, 64, 64);
    	SCHOOT = GameBoard.getInstance().getAnimationsDirectory() + "/u5w.png"; //, 64, 64);
	}

	public static int getGoldCost() {
		return goldCost;
	}

	public static void setGoldCost(int goldCost) {
		Unit5.goldCost = goldCost;
	}

	static {
	    try {
	    	
	    } catch (Exception e) { } 
	} 

	@Override
	public String getWALK() {
		return this.WALK;
	}
	@Override
	public String getSCHOOT() {
		return this.SCHOOT;
	}
	@Override
	public int getSprite1_size() {
		return sprite1_size;
	}
	@Override
	public void setSprite1_size(int sprite1_size) {
		this.sprite1_size = sprite1_size;
	}
	@Override
	public int getSprite2_size() {
		return sprite2_size;
	}

	@Override
	public void setSprite2_size(int sprite2_size) {
		this.sprite2_size = sprite2_size;
	}
}
