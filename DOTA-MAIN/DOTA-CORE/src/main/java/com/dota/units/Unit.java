package com.dota.units;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("serial")
public abstract class Unit implements Serializable {

	protected String WALK;
	protected String SCHOOT;
	public String getWALK() {
		return WALK;
	}

	public void setWALK(String wALK) {
		WALK = wALK;
	}

	public String getSCHOOT() {
		return SCHOOT;
	}

	public void setSCHOOT(String sCHOOT) {
		SCHOOT = sCHOOT;
	}

	protected static int goldCost;
	protected static int unitAttack;
	protected static int unitAttackRange;
	protected static int unitHealth;
	protected int sprite1_size;
	protected int sprite2_size;
	
	public int getSprite1_size() {
		return sprite1_size;
	}

	public void setSprite1_size(int sprite1_size) {
		this.sprite1_size = sprite1_size;
	}

	public int getSprite2_size() {
		return sprite2_size;
	}

	public void setSprite2_size(int sprite2_size) {
		this.sprite2_size = sprite2_size;
	}

	public static int getGoldCost() {
		return goldCost;
	}

	public static int getUnitAttack() {
		if(unitAttack == 0) {
			return 5;
		}
		return unitAttack;
	}

	public static int getUnitAttackRange() {
		return unitAttackRange;
	}

	public static int getUnitHealth() {
		return unitHealth;
	}

	public static void setGoldCost(int goldCost) {
		Unit.goldCost = goldCost;
	}

	public static void setUnitAttack(int unitAttack) {
		Unit.unitAttack = unitAttack;
	}

	public static void setUnitAttackRange(int unitAttackRange) {
		Unit.unitAttackRange = unitAttackRange;
	}
	public static void setUnitHealth(int unitHealth) {
		Unit.unitHealth = unitHealth;
	}
		
		
	protected int unitCurrentHealth;

	protected float x = 100;

	protected static float y = 570;

	public static float getY() {
		return y;
	}

	public static void setY(float y) {
		Unit.y = y;
	}

	protected int number;
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	protected Boolean isMoving;

	protected Boolean canMove;

	protected int currentAnimation;

	protected List<Unit> enemies;

	public Unit() {
		this.enemies = new LinkedList<Unit>();
		this.currentAnimation = 0;
		this.isMoving = true;
		this.canMove = true;
	}

	public void addEnemy(Unit unit) {
		this.enemies.add(unit);
	}

	public void changeState(Boolean isMoving) {
		if (this.isMoving != isMoving) {
			this.isMoving = !this.isMoving;
		}
		if (this.isMoving) {
			this.currentAnimation = 0;
		} else {
			this.currentAnimation = 1;
		}
	}

	public Boolean getCanMove() {
		return canMove;
	}

	public int getCurrentAnimation() {
		return currentAnimation;
	}

	public List<Unit> getEnemies() {
		return enemies;
	}

	public Boolean getIsMoving() {
		return isMoving;
	}

	public int getUnitCurrentHealth() {
		return unitCurrentHealth;
	}

	public float getX() {
		return x;
	}

	public void setCanMove(Boolean canMove) {
		this.canMove = canMove;
	}

	public void setCurrentAnimation(int currentAnimation) {
		this.currentAnimation = currentAnimation;
	}

	public void setEnemies(List<Unit> enemies) {
		this.enemies = enemies;
	}

	public void setIsMoving(Boolean isMoving) {
		this.isMoving = isMoving;
	}

	public void setUnitCurrentHealth(int unitCurrentHealth) {
		this.unitCurrentHealth = unitCurrentHealth;
	}

	public void setX(float x) {
		this.x = x;
	}

	public void shoot(int delta) {
		this.enemies.get(0).setUnitCurrentHealth(this.enemies.get(0).getUnitCurrentHealth() - this.getUnitAttack());
	}

	public void walk(int delta) {
		if (!this.canMove) {
			return;
		}
		if (this.getNumber() == 5) {
			this.x -= 0.1 * delta;
		}
		else {
			this.x += 0.1 * delta;
		}
	}
}
