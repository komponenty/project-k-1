package com.dota.units;

import com.dota.board.GameBoard;
import com.dota.units.Unit;

@SuppressWarnings("serial")
public class Unit4 extends Unit {
	
	protected static int goldCost = 75;
	protected static int unitAttack = 0;
	protected static int unitAttackRange = 0;
	protected static int unitHealth = 0;
	protected int unitCurrentHealth = 0;
	
	public Unit4() {
		super();	
		this.unitCurrentHealth = Unit4.unitHealth;
		this.number = 4;
		WALK = GameBoard.getInstance().getAnimationsDirectory() + "/u4w.png"; //, 64, 64);
    	SCHOOT = GameBoard.getInstance().getAnimationsDirectory() + "/u4s.png"; //, 64, 64);
	}

	static {
	    try {
	    	
	    } catch (Exception e) { } 
	}

	public static int getGoldCost() {
		return goldCost;
	}

	public static int getUnitAttack() {
		return unitAttack;
	}

	public static int getUnitAttackRange() {
		return unitAttackRange;
	}

	public static int getUnitHealth() {
		return unitHealth;
	}

	public static void setGoldCost(int goldCost) {
		Unit4.goldCost = goldCost;
	}

	public static void setUnitAttack(int unitAttack) {
		Unit4.unitAttack = unitAttack;
	}

	public static void setUnitAttackRange(int unitAttackRange) {
		Unit4.unitAttackRange = unitAttackRange;
	}
	public static void setUnitHealth(int unitHealth) {
		Unit4.unitHealth = unitHealth;
	} 	
	
	@Override
	public String getWALK() {
		return this.WALK;
	}
	@Override
	public String getSCHOOT() {
		return this.SCHOOT;
	}
	@Override
	public int getSprite1_size() {
		return sprite1_size;
	}
	@Override
	public void setSprite1_size(int sprite1_size) {
		this.sprite1_size = sprite1_size;
	}
	@Override
	public int getSprite2_size() {
		return sprite2_size;
	}

	@Override
	public void setSprite2_size(int sprite2_size) {
		this.sprite2_size = sprite2_size;
	}
}

