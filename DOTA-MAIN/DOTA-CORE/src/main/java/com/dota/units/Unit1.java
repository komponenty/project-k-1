package com.dota.units;

import com.dota.board.GameBoard;
import com.dota.units.Unit;

@SuppressWarnings("serial")
public class Unit1 extends Unit {
	
	protected static int goldCost = 20;
	protected static int unitAttack = 0;
	protected static int unitAttackRange = 0;
	protected static int unitHealth = 0;
	protected int unitCurrentHealth = 0;
	
	public Unit1() {
		super();
		this.unitCurrentHealth = Unit1.unitHealth;
		this.number = 1;
		WALK = GameBoard.getInstance().getAnimationsDirectory() + "/u1w.png"; //, 66, 66;
    	SCHOOT = GameBoard.getInstance().getAnimationsDirectory() + "/u1s.png"; //, 64, 64);
	}
	
	public static int getGoldCost() {
		return goldCost;
	}

	public static int getUnitAttack() {
		return unitAttack;
	}

	public static int getUnitAttackRange() {
		return unitAttackRange;
	}

	public static int getUnitHealth() {
		return unitHealth;
	}

	public static void setGoldCost(int goldCost) {
		Unit1.goldCost = goldCost;
	}

	public static void setUnitAttack(int unitAttack) {
		Unit1.unitAttack = unitAttack;
	}

	public static void setUnitAttackRange(int unitAttackRange) {
		Unit1.unitAttackRange = unitAttackRange;
	}
	public static void setUnitHealth(int unitHealth) {
		Unit1.unitHealth = unitHealth;
	}
		
		
	
	@Override
	public String getWALK() {
		return this.WALK;
	}
	@Override
	public String getSCHOOT() {
		return this.SCHOOT;
	}
	@Override
	public int getSprite1_size() {
		return sprite1_size;
	}
	@Override
	public void setSprite1_size(int sprite1_size) {
		this.sprite1_size = sprite1_size;
	}
	@Override
	public int getSprite2_size() {
		return sprite2_size;
	}

	@Override
	public void setSprite2_size(int sprite2_size) {
		this.sprite2_size = sprite2_size;
	}
}
