package com.dota.contrib.chat.server;

import java.io.*;
import java.net.*;


public class Server {
	public static void main(String[] args) throws IOException {
		final int port = 444;
		ServerSocket ss = new ServerSocket(port);
		Socket clientSocket = ss.accept();
		RecieveFromClientThread recieve = new RecieveFromClientThread(clientSocket);
		Thread thread = new Thread(recieve);
		thread.start();
		SendToClientThread send = new SendToClientThread(clientSocket);
		Thread thread2 = new Thread(send);
		thread2.start();
	}
}