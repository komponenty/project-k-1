package com.dota.contrib.chat.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

class SendToClientThread implements Runnable {
	PrintWriter pwPrintWriter;
	Socket clientSock = null;

	public SendToClientThread(Socket clientSock) {
		this.clientSock = clientSock;
	}

	public void run() {
		try {
			pwPrintWriter = new PrintWriter(new OutputStreamWriter(
					this.clientSock.getOutputStream()));
			while (true) {
				String msgToClientString = null;
				BufferedReader input = new BufferedReader(
						new InputStreamReader(System.in));
				msgToClientString = input.readLine();
				pwPrintWriter.println(msgToClientString);
				pwPrintWriter.flush();
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
}