package com.dota.contrib.chat.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

import com.dota.board.GameSettings;

class RecieveFromClientThread implements Runnable {
	Socket clientSocket = null;
	BufferedReader brBufferedReader = null;

	public RecieveFromClientThread(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	public void run() {
		try {
			brBufferedReader = new BufferedReader(new InputStreamReader(
					this.clientSocket.getInputStream()));
			String messageString;
			while (true) {
				while ((messageString = brBufferedReader.readLine()) != null) {
					if (messageString.equals("EXIT")) {
						break;
					}
					GameSettings.getInstance().getMessages().add(messageString);
					System.out.print(messageString);
				}
				this.clientSocket.close();
				System.exit(0);
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
}