@echo off
echo %CD%
cd /D DOTA-MAIN
call ..\bin\mvn.bat com.zenjava:javafx-maven-plugin:2.0:fix-classpath -DsilentJfxFix=true
call ..\bin\mvn.bat package
call ..\bin\mvn.bat install
cd /D DOTA-MENU
call ..\..\bin\mvn.bat jfx:native
cd /D target\jfx\native
start java -jar DOTA-MENU-jfx.jar
